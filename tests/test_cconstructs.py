# -*- coding: utf-8 -*-
import numpy as np
import pytest
import sympy as sp

from csnake import cconstructs as cc
from csnake.cconstructs import _get_variable
from csnake.cconstructs import _shape
from csnake.codewriterlite import CodeWriterLite


class TestTypes:
    def test_c_array_literal(self):
        intended_type = cc.CArrayLiteral
        assert not isinstance("test", intended_type)
        assert not isinstance({"test": "dict"}, intended_type)
        assert not isinstance((a * a for a in range(10)), intended_type)
        assert not isinstance(True, intended_type)
        assert not isinstance(None, intended_type)

        assert isinstance([1, 2], intended_type)
        assert isinstance((1, 2), intended_type)
        assert isinstance(range(10), intended_type)
        assert isinstance([a * a for a in range(10)], intended_type)

        assert isinstance(np.arange(24).reshape((2, 3, 4)), intended_type)
        assert isinstance(sp.Array([[1, 2], [3, 4], [5, 6]]), intended_type)

    def test_c_struct_literal(self):
        intended_type = cc.CStructLiteral
        assert not isinstance([1, 2], intended_type)
        assert not isinstance((1, 2), intended_type)
        assert not isinstance(range(10), intended_type)
        assert not isinstance("teststr", intended_type)
        assert not isinstance(True, intended_type)
        assert not isinstance(None, intended_type)

        assert not isinstance(np.arange(24).reshape((2, 3, 4)), intended_type)
        assert not isinstance(
            sp.Array([[1, 2], [3, 4], [5, 6]]), intended_type
        )

        assert isinstance({"test": "dict"}, intended_type)
        assert isinstance({a: "test" for a in range(10)}, intended_type)

    def test_int_literal(self):
        intended_type = cc.CIntLiteral
        assert isinstance(1, intended_type)
        assert isinstance(sp.sympify(0), intended_type)
        assert isinstance(sp.sympify(1), intended_type)
        assert isinstance(np.int_(1), intended_type)

        assert not isinstance(1.1, intended_type)
        assert not isinstance(sp.sympify(0.0), intended_type)
        assert not isinstance(sp.sympify(1.1), intended_type)
        assert not isinstance(np.float_(1.1), intended_type)

        assert not isinstance("1", intended_type)
        assert not isinstance("1.0", intended_type)
        assert not isinstance([1], intended_type)
        assert not isinstance((1,), intended_type)
        assert not isinstance(None, intended_type)

    def test_float_literal(self):
        intended_type = cc.CFloatLiteral
        assert not isinstance(1, intended_type)
        assert not isinstance(sp.sympify(0), intended_type)
        assert not isinstance(sp.sympify(1), intended_type)
        assert not isinstance(np.int_(1), intended_type)

        assert isinstance(1.1, intended_type)
        assert isinstance(sp.sympify(0.0), intended_type)
        assert isinstance(sp.sympify(1.1), intended_type)
        assert isinstance(np.float_(1.1), intended_type)

        assert not isinstance("1", intended_type)
        assert not isinstance("1.0", intended_type)
        assert not isinstance([1], intended_type)
        assert not isinstance((1,), intended_type)
        assert not isinstance(True, intended_type)
        assert not isinstance(None, intended_type)

    def test_c_basic_literal(self):
        intended_type = cc.CBasicLiteral
        assert isinstance(1, intended_type)
        assert isinstance(sp.sympify(0), intended_type)
        assert isinstance(sp.sympify(1), intended_type)
        assert isinstance(np.int_(1), intended_type)

        assert isinstance(1.1, intended_type)
        assert isinstance(sp.sympify(0.0), intended_type)
        assert isinstance(sp.sympify(1.1), intended_type)
        assert isinstance(np.float_(1.1), intended_type)

        assert not isinstance("1", intended_type)
        assert not isinstance("1.0", intended_type)
        assert not isinstance([1], intended_type)
        assert not isinstance((1,), intended_type)
        assert not isinstance(None, intended_type)

    def test_c_extended_literal(self):
        intended_type = cc.CExtendedLiteral
        assert isinstance(1, intended_type)
        assert isinstance(sp.sympify(0), intended_type)
        assert isinstance(sp.sympify(1), intended_type)
        assert isinstance(np.int_(1), intended_type)

        assert isinstance(1.1, intended_type)
        assert isinstance(sp.sympify(0.0), intended_type)
        assert isinstance(sp.sympify(1.1), intended_type)
        assert isinstance(np.float_(1.1), intended_type)

        assert isinstance(True, intended_type)
        assert isinstance("test", intended_type)

        assert not isinstance([1], intended_type)
        assert not isinstance((1,), intended_type)
        assert not isinstance(None, intended_type)

    def test_c_literal(self):
        intended_type = cc.CLiteral
        assert isinstance(1, intended_type)
        assert isinstance(sp.sympify(0), intended_type)
        assert isinstance(sp.sympify(1), intended_type)
        assert isinstance(np.int_(1), intended_type)

        assert isinstance(1.1, intended_type)
        assert isinstance(sp.sympify(0.0), intended_type)
        assert isinstance(sp.sympify(1.1), intended_type)
        assert isinstance(np.float_(1.1), intended_type)

        assert isinstance(True, intended_type)
        assert isinstance("test", intended_type)

        assert isinstance([1], intended_type)
        assert isinstance((1,), intended_type)
        assert isinstance({"test": "dict"}, intended_type)

        assert not isinstance(None, intended_type)
        assert not isinstance((a * a for a in range(10)), intended_type)


class TestShape:
    def test_str(self):
        with pytest.raises(cc.ShapelessError):
            _shape("test")

    def test_dict(self):
        dct = {"a": 1, "b": 2}

        with pytest.raises(cc.ShapelessError):
            _shape(dct)

    def test_np_matrix(self):
        npm = np.matrix("1 2; 3 4")
        assert _shape(npm) == npm.shape

    def test_np_ndarray(self):
        npa = np.array([[1, 2, 3], [4, 5, 6]], np.int32)
        assert _shape(npa) == npa.shape

    def test_np_array(self):
        npa = np.arange(15).reshape(3, 5)
        assert _shape(npa) == npa.shape

    def test_sp_array(self):
        spa = sp.Array(range(12), (3, 4))
        assert _shape(spa) == spa.shape

    def test_sp_matrix(self):
        speye = sp.zeros(2, 3)
        assert _shape(speye) == speye.shape

    def test_list_1d(self):
        num = 10
        lst = list(range(num))

        assert _shape(lst) == (num,)

    def test_tuple_1d(self):
        num = 10
        lst = tuple(range(num))

        assert _shape(lst) == (num,)

    def test_list_2d_nonfull_first_row_shorter(self):
        lst = [[1], [2, 3], [4, 5]]
        assert _shape(lst) == (3, 1)

    def test_list_2d_nonfull_second_row_shorter(self):
        lst = [[1, 2], [3], [4, 5]]
        assert _shape(lst) == (3, 2)

    def test_list_2d_full(self):
        lst = [[1, 2], [3, 4], [5, 6]]
        assert _shape(lst) == (3, 2)


@pytest.fixture(scope="class")
def enum_fixture():
    name = "somename"
    pfx = "pfx"
    typedef = False
    fix = cc.Enum(name, pfx, typedef)
    return fix


class TestEnum:
    def test_enum_init(self):
        name = "somename"
        pfx = "pfx"
        typedef = False

        newenum = cc.Enum(name, prefix=pfx, typedef=typedef)
        assert newenum.typedef == typedef
        assert newenum.name == name
        assert newenum.prefix == pfx

    def test_enum_add_single_ok(self):
        name = "somename"
        pfx = "pfx"
        typedef = False
        newenum = cc.Enum(name, prefix=pfx, typedef=typedef)
        newenum.add_value("val1")
        newenum.add_value("val2", 1)
        newenum.add_value("val3", 1, "some comment")

        print(newenum)
        intended_result = """\
enum somename
{
    pfxval1,
    pfxval2 = 1,
    pfxval3 = 1 /* some comment */
};"""

        assert str(newenum) == intended_result

    def test_enum_cvalue(self):
        name = "somename"
        pfx = "pfx"
        typedef = False
        newenum = cc.Enum(name, prefix=pfx, typedef=typedef)

        cval1 = cc.Variable("varname", "int")

        newenum.add_value("val1", cc.Dereference(1000))
        newenum.add_value("val2", cval1)
        newenum.add_value("val3", cc.AddressOf(cval1), "some comment")

        print(newenum)

        intended_result = """\
enum somename
{
    pfxval1 = *1000,
    pfxval2 = varname,
    pfxval3 = &varname /* some comment */
};"""
        assert str(newenum) == intended_result


class TestVariable:
    def test_init_primitive_int(self):
        int_var = cc.Variable("test", primitive="int", value=2)
        assert int_var.declaration == "int test;"
        assert int_var.initialization == "int test = 2;"

    def test_init_primitive_float(self):
        float_var = cc.Variable("test", primitive="float", value=2.0)
        assert float_var.declaration == "float test;"
        assert float_var.initialization == "float test = 2.0;"

    def test_init_primitive_bool(self):
        bool_var = cc.Variable("test", primitive="bool", value=False)
        assert bool_var.declaration == "bool test;"
        assert bool_var.initialization == "bool test = false;"

    def test_init_primitive_str(self):
        str_var = cc.Variable("test", primitive="char", value="abc")
        assert str_var.declaration == "char test[];"
        assert str_var.initialization == 'char test[] = "abc";'

    def test_gen_init_1d_array(self):
        var = cc.Variable("test", primitive="int", value=range(3))
        assert str(var.generate_initialization()) == "int test[3] = {0, 1, 2};"

    def test_gen_init_2d_array(self):
        var = cc.Variable("test", primitive="int", value=[range(3), range(3)])
        print(var.generate_initialization())
        intended_result = """\
int test[2][3] = {
    {0, 1, 2},
    {0, 1, 2}
};"""
        assert str(var.generate_initialization()) == intended_result

    def test_gen_init_nested_2d_arrays(self):
        var = cc.Variable(
            "test", primitive="int", value=np.arange(24).reshape((2, 3, 4))
        )
        print(var.generate_initialization())
        intended_result = """\
int test[2][3][4] = {
    {
        {0, 1, 2, 3},
        {4, 5, 6, 7},
        {8, 9, 10, 11}
    },
    {
        {12, 13, 14, 15},
        {16, 17, 18, 19},
        {20, 21, 22, 23}
    }
};"""
        assert str(var.generate_initialization()) == intended_result

    def test_gen_init_flat_struct(self):
        var = cc.Variable(
            "test", primitive="int", value={"field1": 2, "field2": 3}
        )
        print(var.generate_initialization())
        intended_result = """\
int test = {
    .field1 = 2,
    .field2 = 3
};"""
        assert str(var.generate_initialization()) == intended_result

    def test_gen_init_mixed_struct(self):
        var = cc.Variable(
            "test",
            primitive="int",
            value={
                "field1": 2,
                "field2": {"a": 5, "b": 10},
                "field3": [4, 5, 6],
                "field4": 8,
            },
        )
        print(var.generate_initialization())
        intended_result = """\
int test = {
    .field1 = 2,
    .field2 = {
        .a = 5,
        .b = 10
    },
    .field3 = {4, 5, 6},
    .field4 = 8
};"""
        assert str(var.generate_initialization()) == intended_result

    def test_gen_init_mixed_struct_form(self):
        var = cc.Variable(
            "test",
            primitive="int",
            value={
                "field1": 2,
                "field2": {"a": 5, "b": 10},
                "field3": cc.FormattedLiteral([30, 31, 32], int_formatter=hex),
                "field4": 8,
            },
        )
        print(var.generate_initialization())
        intended_result = """\
int test = {
    .field1 = 2,
    .field2 = {
        .a = 5,
        .b = 10
    },
    .field3 = {0x1e, 0x1f, 0x20},
    .field4 = 8
};"""
        assert str(var.generate_initialization()) == intended_result

    def test_gen_init_mixed_struct_nested_form(self):
        var = cc.Variable(
            "test",
            primitive="int",
            value={
                "field1": [
                    [2, 3],
                    cc.FormattedLiteral(
                        [4, cc.FormattedLiteral(11, int_formatter=oct)],
                        int_formatter=bin,
                    ),
                ],
                "field2": cc.FormattedLiteral(
                    {"a": cc.FormattedLiteral(5, int_formatter=int), "b": 10},
                    int_formatter=bin,
                ),
                "field3": cc.FormattedLiteral([30, 31, 32], int_formatter=hex),
                "field4": 8,
            },
        )
        print(var.generate_initialization())
        intended_result = """\
int test = {
    .field1 = {
        {2, 3},
        {0b100, 0o13}
    },
    .field2 = {
        .a = 5,
        .b = 0b1010
    },
    .field3 = {0x1e, 0x1f, 0x20},
    .field4 = 8
};"""
        assert str(var.generate_initialization()) == intended_result

    def test_declaration_property(self):
        var = cc.Variable(
            "test",
            primitive="int",
            value={
                "field1": [
                    [2, 3],
                    cc.FormattedLiteral(
                        [4, cc.FormattedLiteral(11, int_formatter=oct)],
                        int_formatter=bin,
                    ),
                ],
                "field2": cc.FormattedLiteral(
                    {"a": cc.FormattedLiteral(5, int_formatter=int), "b": 10},
                    int_formatter=bin,
                ),
                "field3": cc.FormattedLiteral([30, 31, 32], int_formatter=hex),
                "field4": 8,
            },
        )

        assert var.generate_declaration(extern=False) + ";" == var.declaration

    def test_declaration_with_shape(self):
        var = cc.Variable("test", primitive="int", array=(2, 3))
        assert var.declaration == "int test[2][3];"

    def test_initialization_without_value_fail(self):
        var = cc.Variable("test", primitive="int", array=(2, 3))
        with pytest.raises(cc.NoValueError):
            var.initialization

    def test_initialization_property(self):
        var = cc.Variable(
            "test",
            primitive="int",
            value={
                "field1": [
                    [2, 3],
                    cc.FormattedLiteral(
                        [4, cc.FormattedLiteral(11, int_formatter=oct)],
                        int_formatter=bin,
                    ),
                ],
                "field2": cc.FormattedLiteral(
                    {"a": cc.FormattedLiteral(5, int_formatter=int), "b": 10},
                    int_formatter=bin,
                ),
                "field3": cc.FormattedLiteral([30, 31, 32], int_formatter=hex),
                "field4": 8,
            },
        )

        assert (
            var.generate_initialization(indent="    ").code
            == var.initialization
        )

    def test_zero_level_formatted_literal(self):
        var = cc.Variable(
            "test",
            primitive="int",
            value=cc.FormattedLiteral([1, 2, 3], int_formatter=hex),
        )

        assert (
            var.generate_initialization(indent="    ").code
            == "int test[3] = {0x1, 0x2, 0x3};"
        )

    def test_zero_level_formatted_literal_multiformat(self):
        var = cc.Variable(
            "test",
            primitive="int",
            value=cc.FormattedLiteral(
                cc.FormattedLiteral([1, 2, 3], int_formatter=hex),
                int_formatter=oct,
            ),
        )

        assert (
            var.generate_initialization(indent="    ").code
            == "int test[3] = {0x1, 0x2, 0x3};"
        )

    def test_str(self):
        # variable with value
        var1 = cc.Variable(
            "test",
            primitive="int",
            value={
                "field1": [
                    [2, 3],
                    cc.FormattedLiteral(
                        [4, cc.FormattedLiteral(11, int_formatter=oct)],
                        int_formatter=bin,
                    ),
                ],
                "field2": cc.FormattedLiteral(
                    {"a": cc.FormattedLiteral(5, int_formatter=int), "b": 10},
                    int_formatter=bin,
                ),
                "field3": cc.FormattedLiteral([30, 31, 32], int_formatter=hex),
                "field4": 8,
            },
        )
        assert var1.generate_initialization(indent="    ").code == str(var1)

        # variable without value
        var2 = cc.Variable("test", primitive="int")
        assert var2.declaration == str(var2)

    def test_init_other_var(self):
        other_var = cc.Variable("other_var", primitive="int", value=1)
        this_var = cc.Variable("test", primitive="int*", value=other_var)
        assert this_var.declaration == "int* test;"
        assert this_var.initialization == "int* test = other_var;"

    def test_init_addressof(self):
        other_var = cc.Variable("other_var", primitive="int", value=1)
        this_var = cc.Variable(
            "test", primitive="int*", value=cc.AddressOf(other_var)
        )
        assert this_var.declaration == "int* test;"
        assert this_var.initialization == "int* test = &other_var;"

    def test_init_dereference(self):
        other_var = cc.Variable("other_var", primitive="int*")
        this_var = cc.Variable(
            "test", primitive="int", value=cc.Dereference(other_var)
        )
        assert this_var.declaration == "int test;"
        assert this_var.initialization == "int test = *other_var;"

    def test_init_typecast(self):
        other_var = cc.Variable("other_var", primitive="float")
        this_var = cc.Variable(
            "test", primitive="int", value=cc.Typecast(other_var, "int")
        )
        assert this_var.declaration == "int test;"
        assert this_var.initialization == "int test = (int) other_var;"

    def test_init_1d_subscript(self):
        other_var = cc.Variable("other_var", primitive="int", value=(1, 2, 3))
        this_var = cc.Variable(
            "test", primitive="int", value=cc.Subscript(other_var, 1)
        )
        assert this_var.declaration == "int test;"
        assert this_var.initialization == "int test = other_var[1];"

    def test_init_2d_subscript(self):
        other_var = cc.Variable(
            "other_var", primitive="int", value=((1, 2, 3), (4, 5, 6))
        )
        this_var = cc.Variable(
            "test", primitive="int", value=cc.Subscript(other_var, (0, 1))
        )
        assert this_var.declaration == "int test;"
        assert this_var.initialization == "int test = other_var[0][1];"

    def test_init_2d_subscript_var(self):
        other_var = cc.Variable(
            "other_var", primitive="int", value=((1, 2, 3), (4, 5, 6))
        )
        index_var = cc.Variable("index", primitive="int", value=5)
        this_var = cc.Variable(
            "test",
            primitive="int",
            value=cc.Subscript(other_var, (0, index_var)),
        )
        assert this_var.declaration == "int test;"
        assert this_var.initialization == "int test = other_var[0][index];"

    def test_init_dot(self):
        other_var = cc.Variable("other_var", primitive="struct some")
        this_var = cc.Variable(
            "test", primitive="int", value=cc.Dot(other_var, "some_field")
        )
        assert this_var.declaration == "int test;"
        assert this_var.initialization == "int test = other_var.some_field;"

    def test_init_arrow(self):
        other_var = cc.Variable("other_var", primitive="struct some*")
        this_var = cc.Variable(
            "test", primitive="int", value=cc.Arrow(other_var, "some_field")
        )
        assert this_var.declaration == "int test;"
        assert this_var.initialization == "int test = other_var->some_field;"

    def test_init_generic_modifier(self):
        other_var = cc.Variable("other_var", primitive="int")
        this_var = cc.Variable(
            "test",
            primitive="int",
            value=cc.GenericModifier(other_var, lambda l: f"SOME_MACRO({l})"),
        )
        assert this_var.declaration == "int test;"
        assert this_var.initialization == "int test = SOME_MACRO(other_var);"

    def test_init_offset_of(self):
        struct = cc.Struct("some")
        this_var = cc.Variable(
            "test", primitive="size_t", value=cc.OffsetOf(struct, "some_field")
        )
        assert this_var.declaration == "size_t test;"
        assert (
            this_var.initialization
            == "size_t test = offsetof(struct some, some_field);"
        )

    def test_init_text_modifier(self):
        this_var = cc.Variable(
            "test", primitive="int", value=cc.TextModifier("random_text")
        )
        assert this_var.declaration == "int test;"
        assert this_var.initialization == "int test = random_text;"


class TestStruct:
    def test_init_ok(self):
        cc.Struct("strname", typedef=False)

    def test_init_notok(self):
        with pytest.raises(TypeError):
            cc.Struct(2, typedef=False)
        with pytest.raises(TypeError):
            cc.Struct("strname", typedef=False, comment=2)

    def test_add(self):
        new_struct = cc.Struct("strname", typedef=False)
        var1 = cc.Variable("var1", "int")
        var2 = cc.Variable("var2", "int", value=range(10))
        new_struct.add_variable(var1)
        new_struct.add_variable(var2)
        new_struct.add_variable(("var3", "int"))
        new_struct.add_variable({"name": "var4", "primitive": "int"})

        assert (
            str(new_struct)
            == """\
struct strname
{
    int var1;
    int var2[10];
    int var3;
    int var4;
};"""
        )

    def test_str(self):
        new_struct = cc.Struct("strname", typedef=False)
        arg1 = cc.Variable("arg1", "int")
        arg2 = cc.Variable("arg2", "int", value=range(10))
        new_struct.add_variable(arg1)
        new_struct.add_variable(arg2)
        new_struct.add_variable(("arg3", "int"))
        new_struct.add_variable({"name": "arg4", "primitive": "int"})

        assert str(new_struct) == new_struct.generate_declaration().code


class TestGetVariable:
    def test_variable(self):
        var1 = cc.Variable("var1", "int")
        var2 = cc.Variable("var2", "int", value=range(10))

        assert var1 == _get_variable(var1)
        assert var2 == _get_variable(var2)

    def test_tuple(self):
        var3 = ("var3", "int")
        var3_get = cc._get_variable(var3)
        assert (var3_get.name, var3_get.primitive) == var3

    def test_dict(self):
        var4 = {"name": "var4", "primitive": "int"}
        var4_get = cc._get_variable(var4)
        assert {"name": var4_get.name, "primitive": var4_get.primitive} == var4


class TestFunction:
    def test_init_ok(self):
        cc.Function("testfunct", "void", "static")
        cc.Function("testfunct", "void", "static inline")
        cc.Function("testfunct", "void", ("static", "inline"))
        cc.Function("testfunct")
        cc.Function("testfunct", arguments=(cc.Variable("var", "int"),))

    def test_init_notok(self):
        with pytest.raises(TypeError):
            cc.Function(2, "void", "static")
        with pytest.raises(TypeError):
            cc.Function("testfunct", 2, "static")
        with pytest.raises(TypeError):
            cc.Function("testfunct", 2, "static")
        with pytest.raises(TypeError):
            cc.Function("testfunct", "void", 2)
        with pytest.raises(TypeError):
            cc.Function("testfunct", "void", ["static", 2])

    def test_init_qualifiers(self):
        qual_str = "static inline"
        qual_arr = ["static", "inline"]

        qual_str_func = cc.Function("testfunct", "void", qual_str)
        assert qual_str_func.qualifiers == qual_arr

        qual_arr_func = cc.Function("testfunct", "void", qual_arr)
        assert qual_arr_func.qualifiers == qual_arr

        qual_none_func = cc.Function("testfunct", "void")
        assert qual_none_func.qualifiers == []

    def test_add_arguments_on_init_ok(self):
        arg1 = cc.Variable("arg1", "int")
        arg2 = cc.Variable("arg2", "int", value=range(10))
        arg3 = ("arg3", "int")
        arg4 = {"name": "arg4", "primitive": "int"}
        fun = cc.Function(
            "testfunct", "void", arguments=(arg1, arg2, arg3, arg4)
        )
        assert (
            fun.prototype
            == "void testfunct(int arg1, int arg2[10], int arg3, int arg4);"
        )

    def test_add_arguments_on_ok(self):
        arg1 = cc.Variable("arg1", "int")
        arg2 = cc.Variable("arg2", "int", value=range(10))
        arg3 = ("arg3", "int")
        arg4 = {"name": "arg4", "primitive": "int"}
        fun = cc.Function("testfunct", "void")
        fun.add_arguments((arg1, arg2, arg3, arg4))
        assert (
            fun.prototype
            == "void testfunct(int arg1, int arg2[10], int arg3, int arg4);"
        )

    def test_add_argument_on_ok(self):
        arg1 = cc.Variable("arg1", "int")
        arg2 = cc.Variable("arg2", "int", value=range(10))
        arg3 = ("arg3", "int")
        arg4 = {"name": "arg4", "primitive": "int"}
        fun = cc.Function("testfunct", "void")
        fun.add_argument(arg1)
        fun.add_argument(arg2)
        fun.add_argument(arg3)
        fun.add_argument(arg4)
        assert (
            fun.prototype
            == "void testfunct(int arg1, int arg2[10], int arg3, int arg4);"
        )

    def test_add_code_str(self):
        arg1 = cc.Variable("arg1", "int")
        arg2 = cc.Variable("arg2", "int", value=range(10))
        arg3 = ("arg3", "int")
        arg4 = {"name": "arg4", "primitive": "int"}
        fun = cc.Function(
            "testfunct", "void", arguments=(arg1, arg2, arg3, arg4)
        )
        fun.add_code("code;")
        assert (
            fun.definition
            == """\
void testfunct(int arg1, int arg2[10], int arg3, int arg4)
{
    code;
}"""
        )

    def test_add_code_str_list(self):
        arg1 = cc.Variable("arg1", "int")
        arg2 = cc.Variable("arg2", "int", value=range(10))
        arg3 = ("arg3", "int")
        arg4 = {"name": "arg4", "primitive": "int"}
        fun = cc.Function(
            "testfunct", "void", arguments=(arg1, arg2, arg3, arg4)
        )
        fun.add_code(("code;", "more_code;"))
        assert (
            fun.definition
            == """\
void testfunct(int arg1, int arg2[10], int arg3, int arg4)
{
    code;
    more_code;
}"""
        )

    def test_add_code_codewriter(self):
        arg1 = cc.Variable("arg1", "int")
        arg2 = cc.Variable("arg2", "int", value=range(10))
        arg3 = ("arg3", "int")
        arg4 = {"name": "arg4", "primitive": "int"}
        fun = cc.Function(
            "testfunct", "void", arguments=(arg1, arg2, arg3, arg4)
        )
        cw = CodeWriterLite()
        cw.add_lines(("code;", "more_code;"))
        fun.add_code(cw)
        assert (
            fun.definition
            == """\
void testfunct(int arg1, int arg2[10], int arg3, int arg4)
{
    code;
    more_code;
}"""
        )

    def test_prototype_property(self):
        arg1 = cc.Variable("arg1", "int")
        arg2 = cc.Variable("arg2", "int", value=range(10))
        arg3 = ("arg3", "int")
        arg4 = {"name": "arg4", "primitive": "int"}
        fun = cc.Function(
            "testfunct", "void", arguments=(arg1, arg2, arg3, arg4)
        )
        fun.add_code(("code;", "more_code;"))
        assert fun.prototype == fun.generate_prototype() + ";"

    def test_definition_property(self):
        arg1 = cc.Variable("arg1", "int")
        arg2 = cc.Variable("arg2", "int", value=range(10))
        arg3 = ("arg3", "int")
        arg4 = {"name": "arg4", "primitive": "int"}
        fun = cc.Function(
            "testfunct", "void", arguments=(arg1, arg2, arg3, arg4)
        )
        fun.add_code(("code;", "more_code;"))
        assert fun.definition == str(fun.generate_definition())

    def test_generate_call_notok(self):
        arg1 = cc.Variable("arg1", "int")
        arg2 = cc.Variable("arg2", "int", value=range(10))
        arg3 = ("arg3", "int")
        arg4 = {"name": "arg4", "primitive": "int"}

        fun = cc.Function(
            "testfunct", "void", arguments=(arg1, arg2, arg3, arg4)
        )

        with pytest.raises(ValueError):
            fun.generate_call(2, 3, 4)

        with pytest.raises(ValueError):
            fun.generate_call([2, 3, 4])

    def test_generate_call_single_arg_not_iter(self):
        arg1 = cc.Variable("arg1", "int")
        fun = cc.Function("testfunct", "void", arguments=([arg1]))

        assert fun.generate_call(2) == "testfunct(2)"

    def test_generate_call(self):
        arg1 = cc.Variable("arg1", "int")
        arg2 = cc.Variable("arg2", "int", value=range(10))
        arg3 = ("arg3", "int")
        arg4 = {"name": "arg4", "primitive": "int"}
        fun = cc.Function(
            "testfunct", "void", arguments=(arg1, arg2, arg3, arg4)
        )

        assert fun.generate_call(2, 3, 4, 5) == "testfunct(2, 3, 4, 5)"
        assert fun.generate_call([2, 3, 4, 5]) == "testfunct(2, 3, 4, 5)"


class TestFuncPtrVariable:
    def test_funct_ptr_init(self):
        fp = cc.FuncPtr("int", (("arg1", "int"), ("arg2", "int")))
        fun = cc.Function(
            "testfunct", "void", arguments=(("arg1", "int"), ("arg2", "int"))
        )
        var = cc.Variable("fptr", fp, value=fun)
        assert var.declaration == "int (*fptr)(int arg1, int arg2);"
        assert (
            var.initialization
            == "int (*fptr)(int arg1, int arg2) = testfunct;"
        )

    def test_funct_ptr_inside_struct(self):
        new_struct = cc.Struct("strname", typedef=False)
        fp = cc.FuncPtr("int", (("arg1", "int"), ("arg2", "int")))
        var1 = cc.Variable("fptr", fp)
        var2 = cc.Variable("fptrs", fp, array=2)
        new_struct.add_variable(var1)
        new_struct.add_variable(var2)
        assert (
            str(new_struct)
            == """\
struct strname
{
    int (*fptr)(int arg1, int arg2);
    int (*fptrs[2])(int arg1, int arg2);
};"""
        )
