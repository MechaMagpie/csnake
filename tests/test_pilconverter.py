# -*- coding: utf-8 -*-
import sys

import pytest

# from csnake.pil_converter import pil_image_to_list
# from csnake.utils import rgb888_to_rgb565, bytes_to_int
# from PIL import Image, ImageDraw
# from csnake.pil_converter import pil_image_to_list


class TestImport:
    def test_no_pil(self, monkeypatch):

        monkeypatch.setitem(sys.modules, "PIL", "None")

        with pytest.raises(ImportError):
            from csnake import pil_converter  # noqa

    def test_yes_pil(self):
        from csnake import pil_converter

        assert pil_converter.pil_image_to_list


@pytest.fixture
def generic_class_object():
    class Whatever:
        def __init__(self):
            pass

    return Whatever()


@pytest.fixture
def pil_rgb_image():
    from PIL import Image, ImageDraw

    im = Image.new("RGB", (3, 3))
    dr = ImageDraw.Draw(im)
    dr.line((0, 0) + im.size, fill="#8CAFAF")
    return im


class TestGeneration:
    def test_not_image(self, generic_class_object):
        from csnake.pil_converter import pil_image_to_list

        with pytest.raises(TypeError):
            pil_image_to_list(generic_class_object, "RGB888")

    def test_rgb_to_rgb888(self):
        from PIL import Image, ImageDraw
        from csnake.pil_converter import pil_image_to_list
        from csnake.pil_converter import bytes_to_int

        dim_x = 3
        dim_y = 3

        color_888 = (65, 105, 20)
        out_color = bytes_to_int(color_888)

        im = Image.new("RGB", (dim_x, dim_y))
        dr = ImageDraw.Draw(im)

        dr.line((0, 0) + im.size, fill=color_888)

        out = pil_image_to_list(im, "RGB888")

        supposed_out = [[0 for _ in range(dim_x)] for _ in range(dim_y)]
        for a in range(dim_x):
            supposed_out[a][a] = out_color

        assert out == supposed_out

    def test_rgb_to_rgb565(self):
        from PIL import Image, ImageDraw
        from csnake.pil_converter import pil_image_to_list
        from csnake.pil_converter import rgb888_to_rgb565

        dim_x = 3
        dim_y = 3

        color_888 = (65, 105, 20)
        out_color = rgb888_to_rgb565(color_888)

        im = Image.new("RGB", (dim_x, dim_y))
        dr = ImageDraw.Draw(im)

        dr.line((0, 0) + im.size, fill=color_888)

        out = pil_image_to_list(im, "RGB565")

        supposed_out = [[0 for _ in range(dim_x)] for _ in range(dim_y)]
        for a in range(dim_x):
            supposed_out[a][a] = out_color

        assert out == supposed_out

    def test_rgba_to_argb8888(self):
        from PIL import Image, ImageDraw
        from csnake.pil_converter import pil_image_to_list
        from csnake.pil_converter import bytes_to_int

        dim_x = 3
        dim_y = 3

        color_888 = (255, 65, 105, 20)
        out_color = bytes_to_int(color_888)

        im = Image.new("RGBA", (dim_x, dim_y))
        dr = ImageDraw.Draw(im)

        dr.line((0, 0) + im.size, fill=color_888)

        out = pil_image_to_list(im, "ARGB8888")

        supposed_out = [[0 for _ in range(dim_x)] for _ in range(dim_y)]
        for a in range(dim_x):
            supposed_out[a][a] = out_color

        assert out == supposed_out

    def test_l_to_a8(self):
        from PIL import Image, ImageDraw
        from csnake.pil_converter import pil_image_to_list
        from csnake.pil_converter import bytes_to_int

        dim_x = 3
        dim_y = 3

        color = (18,)
        out_color = bytes_to_int(color)

        im = Image.new("L", (dim_x, dim_y))
        dr = ImageDraw.Draw(im)

        dr.line((0, 0) + im.size, fill=color)

        out = pil_image_to_list(im, "A8")

        supposed_out = [[0 for _ in range(dim_x)] for _ in range(dim_y)]
        for a in range(dim_x):
            supposed_out[a][a] = out_color

        assert out == supposed_out

    def test_unknown_color_combo(self, pil_rgb_image):
        from csnake.pil_converter import pil_image_to_list

        with pytest.raises(ValueError):
            pil_image_to_list(pil_rgb_image, "NONEXISTANT")
